﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace BndLinqBoxMuller
{
    /// <summary>
    /// This class provides a LINQ-based implementation of the Box-Muller
    /// transformation.
    /// </summary>
    class LinqBoxMuller
    {
        /// <summary>
        /// A thread-local uniform random number generator.  This uses the
        /// implementation described here:
        ///     http://www.albahari.com/threading/part3.aspx#_ThreadLocalT
        /// </summary>
        readonly ThreadLocal<Random> rnd = new ThreadLocal<Random>
            (() => new Random(Guid.NewGuid().GetHashCode()));


        /// <summary>
        /// Creates an array of double arrays containing normailized random
        /// numbers.
        /// </summary>
        /// <param name="m">Size of the first dimension</param>
        /// <param name="n">Size of the second dimension</param>
        /// <param name="parallel">Set to true for parallel generation
        /// (the default), false for serial</param>
        /// <returns>An array of double arrays containing normailized random
        /// numbers</returns>
        public IEnumerable<double[]> GetDoubles(int m, int n, bool parallel = true)
        {
            var r = Enumerable.Range(0, m);
            if (parallel) return r.AsParallel()
                .Select(a => GetDoubles(n)).ToArray();
            else return r
                .Select(a => GetDoubles(n)).ToArray();
        }


        /// <summary>
        /// Creates an array of doubles containing normailized random
        /// numbers.  Note that parallelization has minimal affect on
        /// efficiency.
        /// </summary>
        /// <param name="nsteps">Size of the array</param>
        /// <param name="parallel">Set to true for parallel generation,
        /// false for serial (the default)</param>
        /// <returns>An array of doubles containing normailized random
        /// numbers</returns>
        public double[] GetDoubles(int nsteps, bool parallel = false)
        {
            Func<int, double[]> iter = a =>
            {
                var u1 = rnd.Value.NextDouble();
                var u2 = rnd.Value.NextDouble();
                var c1 = 2 * Math.PI * u2;
                var c2 = Math.Sqrt(-2 * Math.Log(u1));
                return new [] { Math.Cos(c1) * c2, Math.Sin(c1) * c2 };
            };

            // 
            int n = (nsteps + 1) / 2;


            var dbl2 = parallel ?
                Enumerable.Range(0, n).AsParallel().Select(iter) :
                Enumerable.Range(0, n)             .Select(iter)
                ;

            return dbl2.Select((p) => p[0])
                .Concat(dbl2.Select((p) => p[1]))
                .Take(nsteps).ToArray();
        }
    }
}
