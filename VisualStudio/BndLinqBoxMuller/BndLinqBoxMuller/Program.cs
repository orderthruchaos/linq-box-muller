﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;

namespace BndLinqBoxMuller
{
    class Program
    {
        static void Main(string[] args)
        {
            int
                  npaths = 100000
                , nsteps = 252
                , nTotal = npaths * nsteps
                ;

            Func<int, bool, int> ParallelRand = (dim, par) => {
                var nrand = new LinqBoxMuller();
                var sw = new Stopwatch();
                sw.Start();

                double avg;
                if (dim == 1)
                {
                    var paths = nrand.GetDoubles(nTotal, par);
                    avg = paths.Sum() / (nTotal);
                } else {
                    var paths = nrand.GetDoubles(npaths, nsteps, par);
                    avg = (paths.Select((p) => p.Sum()).Sum() / (nTotal));
                }
                sw.Stop();
                Console.WriteLine("Dim:  {0},  Avg:  {1},  Time: {2}", dim, avg, sw.ElapsedMilliseconds);

                // Force garbage collection so timings will not be affected.
                GC.Collect();
                return 0;
            };

            ParallelRand(1, true);
            ParallelRand(1, false);

            ParallelRand(2, true);
            ParallelRand(2, false);
            
            Console.ReadLine();
        }
    }
}
