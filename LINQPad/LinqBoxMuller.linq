<Query Kind="Program" />

void Main()
{
    int
          npaths = 100000
        , nsteps = 252
        , nTotal = npaths * nsteps
        ;

    Func<int, bool, int> ParallelRand = (dim, par) => {
        var nrand = new LinqBoxMuller();
        double avg;

        var sw1 = new Stopwatch();
        var sw2 = new Stopwatch();
        sw1.Start(); 
        sw2.Start();
        if (dim == 1)
        {
            var paths = nrand.GetDoubles(nTotal, par);
        	sw1.Stop();
            avg = paths.Sum() / (nTotal);
        } else {
            var paths = nrand.GetDoubles(npaths, nsteps, par);
        	sw1.Stop();
            avg = (paths.Select((p) => p.Sum()).Sum() / (nTotal));
        }
        sw2.Stop();
        Console.WriteLine(
            "Dim:  {0},  Avg:  {1},  Query Time: {2},  Total Time: {2}",
            dim, avg, sw1.ElapsedMilliseconds, sw2.ElapsedMilliseconds
            );

        // Force garbage collection so timings will not be affected.
        GC.Collect();
        return 0;
    };


    // // On older machines may cause out of memory exceptions. 
    // ParallelRand(1, true);
    // ParallelRand(1, false);

    ParallelRand(2, true);
    ParallelRand(2, false);

    // Console.ReadLine();
}


class StdNormalDistRand {
	ThreadLocal<Random> rnd = new ThreadLocal<Random>
		( () => new Random(Guid.NewGuid().GetHashCode()) );
		
		
	public double[][] GetDoublesParallel(int m, int n)
	{
		return Enumerable.Range(0,m).AsParallel()
			.Select((a) => GetDoubles(n)).ToArray();
	}
		
		
	public double[][] GetDoubles(int m, int n)
	{
		return Enumerable.Range(0,m)
			.Select((a) => GetDoubles(n)).ToArray();
	}
	

	public double[] GetDoubles(int nsteps)
	{
		int n = (nsteps + 1) / 2;
		var dbl1 = Enumerable.Range(0,n).Select(
			(a) => {
				var u1 = rnd.Value.NextDouble();
				var u2 = rnd.Value.NextDouble();
				var c1 = 2 * Math.PI * u2;
				var c2 = Math.Sqrt(-2 * Math.Log(u1));
				return new double[] {Math.Cos(c1) * c2, Math.Sin(c1) * c2};
			}
		);
		return dbl1.Select((p) => p[0])
			.Concat(dbl1.Select((p) => p[1]))
			.Take(nsteps).ToArray();
	}

    
	public double[] GetDoublesParallel(int nsteps)
	{
		int n = (nsteps + 1) / 2;
		var dbl1 = Enumerable.Range(0,n).AsParallel().Select(
			(a) => {
				var u1 = rnd.Value.NextDouble();
				var u2 = rnd.Value.NextDouble();
				var c1 = 2 * Math.PI * u2;
				var c2 = Math.Sqrt(-2 * Math.Log(u1));
				return new double[] {Math.Cos(c1) * c2, Math.Sin(c1) * c2};
			}
		);
		return dbl1.Select((p) => p[0])
			.Concat(dbl1.Select((p) => p[1]))
			.Take(nsteps).ToArray();
	}


	public double[] GetDoublesParallel_old(int nsteps)
	{
		int extra;
		int n = Math.DivRem(nsteps, 2, out extra);


		var dbl1 = Enumerable.Range(0,n).AsParallel().Select(
			(a) => {
				var u1 = rnd.Value.NextDouble();
				var u2 = rnd.Value.NextDouble();
				var c1 = 2 * Math.PI * u2;
				var c2 = Math.Sqrt(-2 * Math.Log(u1));
				return new double[] {Math.Cos(c1) * c2, Math.Sin(c1) * c2};
			}
		);

		if (extra == 0)
			return dbl1.Select((p) => p[0])
				.Concat(dbl1.Select((p) => p[1])).ToArray();


		var ext1 = Enumerable.Range(0,extra).AsParallel().Select(
			(a) => {
				var u1 = rnd.Value.NextDouble();
				var u2 = rnd.Value.NextDouble();
				var c1 = 2 * Math.PI * u2;
				var c2 = Math.Sqrt(-2 * Math.Log(u1));
				return Math.Cos(c1) * c2;
			}
		);
		var uni1 = dbl1.Select((p) => p[0])
			.Concat(dbl1.Select((p) => p[1]))
			.Concat(ext1.Select((a) => a));

		return uni1.ToArray();
	}
}

    /// <summary>
    /// This class provides a LINQ-based implementation of the Box-Muller
    /// transformation.
    /// </summary>
    class LinqBoxMuller
    {
        /// <summary>
        /// A thread-local uniform random number generator.  This uses the
        /// implementation described here:
        ///     http://www.albahari.com/threading/part3.aspx#_ThreadLocalT
        /// </summary>
        ThreadLocal<Random> rnd = new ThreadLocal<Random>
            (() => new Random(Guid.NewGuid().GetHashCode()));


        /// <summary>
        /// Creates an array of double arrays containing normailized random
        /// numbers.
        /// </summary>
        /// <param name="m">Size of the first dimension</param>
        /// <param name="n">Size of the second dimension</param>
        /// <param name="parallel">Set to true for parallel generation
        /// (the default), false for serial</param>
        /// <returns>An array of double arrays containing normailized random
        /// numbers</returns>
        public double[][] GetDoubles(int m, int n, bool parallel = true)
        {
            var r = Enumerable.Range(0, m);
            if (parallel) return r.AsParallel()
                .Select((a) => GetDoubles(n, false)).ToArray();
            else return r
                .Select((a) => GetDoubles(n, false)).ToArray();
        }


        /// <summary>
        /// Creates an array of doubles containing normailized random
        /// numbers.  Note that parallelization has minimal affect on
        /// efficiency.
        /// </summary>
        /// <param name="nsteps">Size of the array</param>
        /// <param name="parallel">Set to true for parallel generation,
        /// false for serial (the default)</param>
        /// <returns>An array of doubles containing normailized random
        /// numbers</returns>
        public double[] GetDoubles(int nsteps, bool parallel = false)
        {
            Func<int, double[]> iter = (a) =>
            {
                var u1 = rnd.Value.NextDouble();
                var u2 = rnd.Value.NextDouble();
                var c1 = 2 * Math.PI * u2;
                var c2 = Math.Sqrt(-2 * Math.Log(u1));
                return new double[] { Math.Cos(c1) * c2, Math.Sin(c1) * c2 };
            };

            // 
            int n = (nsteps + 1) / 2;


            var dbl2 = parallel ?
                Enumerable.Range(0, n).AsParallel().Select(iter) :
                Enumerable.Range(0, n)             .Select(iter)
                ;

            return dbl2.Select((p) => p[0])
                .Concat(dbl2.Select((p) => p[1]))
                .Take(nsteps).ToArray();
        }
    }


// vim:ft=cs sw=8 sts=8 noet